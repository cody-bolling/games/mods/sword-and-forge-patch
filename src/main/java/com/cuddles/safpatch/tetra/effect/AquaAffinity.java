package com.cuddles.safpatch.tetra.effect;

import net.minecraft.world.item.ItemStack;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import se.mickelus.tetra.effect.ItemEffect;
import se.mickelus.tetra.items.modular.ModularItem;

public class AquaAffinity {
  public static final ItemEffect aquaAffinity = ItemEffect.get("safpatch.aquaaffinity");

  @SubscribeEvent
  public void breakBlockEvent(PlayerEvent.BreakSpeed event) {
    ItemStack heldStack = event.getEntity().getMainHandItem();

    if (heldStack.getItem() instanceof ModularItem item) {
      int level = item.getEffectLevel(heldStack, aquaAffinity);

      if (level > 0 && event.getEntity().isUnderWater())
      {
        event.setNewSpeed(event.getOriginalSpeed() * 5.0f);
      }
    }
  }
}
