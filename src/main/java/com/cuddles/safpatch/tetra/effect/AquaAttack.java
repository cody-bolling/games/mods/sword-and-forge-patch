package com.cuddles.safpatch.tetra.effect;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.ai.attributes.AttributeInstance;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import se.mickelus.tetra.effect.ItemEffect;
import se.mickelus.tetra.items.modular.ModularItem;

public class AquaAttack {
  public static final ItemEffect aquaAttack = ItemEffect.get("safpatch.aquaattack");

  @SubscribeEvent
  public void attackEvent(LivingHurtEvent event) {
    Entity source = event.getSource().getEntity();

    if (source instanceof Player player) {
      ItemStack heldStack = player.getMainHandItem();
      AttributeInstance attackDamage = player.getAttribute(Attributes.ATTACK_DAMAGE);

      if (heldStack.getItem() instanceof ModularItem item) {
        int level = item.getEffectLevel(heldStack, aquaAttack);

        if (level > 0 && source.isUnderWater() && attackDamage != null)
        {
          event.setAmount((int)attackDamage.getValue() + 4);
        }
      }
    }
  }
}
