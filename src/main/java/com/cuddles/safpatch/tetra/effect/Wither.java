package com.cuddles.safpatch.tetra.effect;

import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import se.mickelus.tetra.effect.ItemEffect;
import se.mickelus.tetra.items.modular.ModularItem;

public class Wither {
  public static final ItemEffect wither = ItemEffect.get("safpatch.wither");

  @SubscribeEvent
  public void attackEvent(LivingHurtEvent event) {
    Entity source = event.getSource().getEntity();

    if (source instanceof Player player) {
      ItemStack heldStack = player.getMainHandItem();

      if (heldStack.getItem() instanceof ModularItem item) {
        int level = item.getEffectLevel(heldStack, wither);

        if (level > 0 && event.getEntity().hasEffect(MobEffects.WITHER) == false)
        {
          event.getEntity().addEffect(new MobEffectInstance(MobEffects.WITHER, 120));
        }
      }
    }
  }
}
