package com.cuddles.safpatch;

import com.cuddles.safpatch.tetra.effect.AquaAffinity;
import com.cuddles.safpatch.tetra.effect.AquaAttack;
import com.cuddles.safpatch.tetra.effect.FireAspect;
import com.cuddles.safpatch.tetra.effect.Slowness;
import com.cuddles.safpatch.tetra.effect.Wither;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;

@Mod(SwordAndForgePatch.MODID)
public class SwordAndForgePatch
{
  public static final String MODID = "safpatch";
  
  public SwordAndForgePatch()
  {
    MinecraftForge.EVENT_BUS.register(this);
    MinecraftForge.EVENT_BUS.register(new AquaAffinity());
    MinecraftForge.EVENT_BUS.register(new AquaAttack());
    MinecraftForge.EVENT_BUS.register(new FireAspect());
    MinecraftForge.EVENT_BUS.register(new Slowness());
    MinecraftForge.EVENT_BUS.register(new Wither());
  }
}
