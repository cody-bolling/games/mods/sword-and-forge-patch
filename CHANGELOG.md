# CHANGELOG

## 0.5

- add mixin support and remove mending enchantment

## 0.4

- move resources to the [Sword and Forge modpack](https://gitlab.com/cody-bolling/games/sword-and-forge) as a data/resource pack

## 0.3

- add tetra materials
  - gem
  - metal
  - socket
  - stone
  - wood
- add effects
  - aqua affinity
  - aqua attack
  - armor piercing
  - fire aspect
  - looting
  - slowness
  - wither
- add tetra improvements

## 0.2

- add dependencies mutil and tetra
- cleanup files and add example effect

## 0.1

- Initial Commit
