![Project Avatar](repo-icon.png)

# Sword and Forge Patch

Minecraft Forge mod for the Sword and Forge modpack.

<details>
<summary>Project Structure</summary>

```text
sword-and-forge-patch
│  README.md
│  CHANGELOG.md
|  .gitlab-ci.yml
|  repo-icon.png
└─ src
   ├─ generated
   └─ main
      ├─ java/com/cuddles/sword_and_forge_patch
      │  └─ SwordAndForgePatch.java
      └─ resources
```

</details>
<br>
